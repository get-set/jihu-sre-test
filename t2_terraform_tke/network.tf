# 定义安全组
resource "tencentcloud_security_group" "sg-jihu_test" {
  name        = "${var.name}-sg"
  description = "${var.name} security group powered by terraform"
}

resource "tencentcloud_security_group_lite_rule" "sg-jihu_test-rule" {
  security_group_id = tencentcloud_security_group.sg-jihu_test.id

  ingress = [
    "ACCEPT#0.0.0.0/0#ALL#ICMP",
    "ACCEPT#0.0.0.0/0#22#TCP",
    "ACCEPT#0.0.0.0/0#8888#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#UDP",
  ]

  egress = [
    "ACCEPT#0.0.0.0/0#ALL#ALL",
  ]
}

# 定义一个 VPC 网络
resource "tencentcloud_vpc" "vpc-jihu_test" {
  name         = "${var.name}-01"
  cidr_block   = "${var.vpc_ip_seg}.0.0/16"
  is_multicast = false

  tags = {
    "user" = var.name
  }
}

# 定义子网，不同的 zone 用不同的子网网段
resource "tencentcloud_subnet" "subnet_a" {
  count             = length(data.tencentcloud_availability_zones_by_product.all_zones.zones)
  name              = "${var.name}-subnet-${count.index}"
  vpc_id            = tencentcloud_vpc.vpc-jihu_test.id
  availability_zone = data.tencentcloud_availability_zones_by_product.all_zones.zones[count.index].name
  cidr_block        = "${var.vpc_ip_seg}.${count.index}.0/24"
  is_multicast      = false
  tags = {
    "user" = var.name
  }
}

