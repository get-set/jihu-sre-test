# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/tencentcloudstack/tencentcloud" {
  version = "1.60.18"
  hashes = [
    "h1:GkVTqav9CSFMKq/7gQkax50JPbcKgFDlLP/g3QBB4m8=",
    "zh:1c42d031814f6e8be085917f6cebe8f2eb97b5b6a9b1c38ddbd39d5039c0e9a2",
    "zh:2097d263a613d8d740e5fa4a9fa2a15cf21b31f02896e7e2b5172bc2b5bc82e6",
    "zh:24f6ef05dc962582915cee6f9a3f66feb9e40061a6e01b58f316919570463d81",
    "zh:39f9dec9db7378d4f8630b54780cd2fe24a3c8700c865a7607862cff8a9a1c68",
    "zh:538f5e680ac1bde63aa704a4b06c122a014cac9fb1730096558b586ad9238fa5",
    "zh:6c751aea780ac6c618340f5027f6e0f353fce931a9f08886d31a7d18150aa715",
    "zh:ad19213e30f3427f91cbb750e66c486af1dc2a8be0f66a1a89aa6e53a67cb6fc",
    "zh:b48789412c7ad1caeacb0794bb5d2f2b20efbb94fdb16aa9f566c9a0ae782dca",
    "zh:bb6613e0e97f99f24de634f1febbfb306c49b561b4df5e1c9e9b082d14f72a9f",
    "zh:c2bfd0d0ab27c14e65f782050338a2f38f2edc541d5cc5d26698c9c2c4c4b2c1",
    "zh:c96832dd3cce13d03f54c4a28808f82bbf3389e9272f58d0a0de358972f5ee49",
    "zh:e014f771fe4b94a3c6ae2cc051e39623aa79a122edf98714320c902ef273474b",
    "zh:e20a7b3b8905d21a76c2d7fc56840e289ce6055b07018b1eb9a5fad2461890b9",
    "zh:f8864ca3de5aec78f04bae7fd53a938ca1d7cef88c2d7127a6f3ac709fa65662",
  ]
}
