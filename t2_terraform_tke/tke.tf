# 创建 TKE 集群
resource "tencentcloud_kubernetes_cluster" "tke-jihu_test" {
  vpc_id                                     = tencentcloud_vpc.vpc-jihu_test.id
  cluster_version                            = var.k8s_ver
  cluster_cidr                               = "${var.pod_ip_seg}.0.0/16"
  cluster_max_pod_num                        = 64
  cluster_name                               = "tke-${var.name}"
  cluster_desc                               = "used for jihu hands-on test, created by terraform"
  cluster_max_service_num                    = 2048
  cluster_internet                           = true
  managed_cluster_internet_security_policies = ["0.0.0.0/0"]
  cluster_deploy_type                        = "MANAGED_CLUSTER"
  cluster_os                                 = "tlinux2.4x86_64"
  container_runtime                          = "containerd"
  deletion_protection                        = false

  worker_config {
    instance_name              = "${var.name}-node"
    availability_zone          = data.tencentcloud_availability_zones_by_product.all_zones.zones[0].name
    instance_type              = var.default_instance_type
    system_disk_type           = "CLOUD_SSD"
    system_disk_size           = 50
    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 1
    public_ip_assigned         = true
    subnet_id                  = tencentcloud_subnet.subnet_a[0].id
    security_group_ids         = [tencentcloud_security_group.sg-jihu_test.id]

    enhanced_security_service = false
    enhanced_monitor_service  = false
    password                  = var.node_password
  }
}

#  创建一个节点池
resource "tencentcloud_kubernetes_node_pool" "node-pool" {
  name                 = "${var.name}-pool"
  cluster_id           = tencentcloud_kubernetes_cluster.tke-jihu_test.id
  max_size             = 10
  min_size             = 1
  vpc_id               = tencentcloud_vpc.vpc-jihu_test.id
  subnet_ids           = [for s in tencentcloud_subnet.subnet_a : s.id]
  retry_policy         = "INCREMENTAL_INTERVALS"
  desired_capacity     = 2
  delete_keep_instance = false
  node_os              = "tlinux2.4x86_64"

  enable_auto_scale    = true
  auto_scaling_config {
    instance_type      = var.default_instance_type
    system_disk_type   = "CLOUD_PREMIUM"
    system_disk_size   = "50"
    security_group_ids = [tencentcloud_security_group.sg-jihu_test.id]

    data_disk {
      disk_type = "CLOUD_PREMIUM"
      disk_size = 50
    }

    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 1
    public_ip_assigned         = true
    password                   = var.node_password
    enhanced_security_service  = false
    enhanced_monitor_service   = false

  }

  labels = {
    "user" = var.name,
  }
}
