# 集群名称
variable "name" {
  default = "jihu_test"
}

# 所在地域
variable "region" {
  default = "ap-shanghai"
}

# k8s 版本
variable "k8s_ver" {
  default = "1.20.6"
}

# pod ip 地址段
variable "pod_ip_seg" {
  default = "172.16"
}

# vpc ip 地址段
variable "vpc_ip_seg" {
  default = "10.0"
}

# 机型
variable "default_instance_type" {
  default = "S2.MEDIUM4"
}

# node 密码
variable "node_password" {
  default = "Cl0ud@Tencent"
}

