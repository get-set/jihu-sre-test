# 腾讯云provider
terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
    }
  }
}
provider "tencentcloud" {
  region = var.region
}

# 查询当前可用区
data "tencentcloud_availability_zones_by_product" "all_zones" {
  product = "cvm"
}

