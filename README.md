# 极狐SRE面试题

提交人：刘康

## 任务1：Service A 的 Dockerfile

> 为该服务开发Dockerfifile，构建容器镜像并发布到镜像仓库（地址不限）。
>
>  [查看Dockerfile](./t1_dockerfile/dockerfile)

- 构建阶段拉取代码。
- 两阶段构建。
- 非root用户运行。

1. 构建Docker镜像（`dockerfile`所在目录下）：

   ```
   docker build -t service-a:0.1.0 .
   ```

2. 上传镜像到Docker Hub：

   ```
   docker login
   docker tag service-a:0.1.0 gets/service-a:0.1.0
   docker push gets/service-a:0.1.0
   ```

## 任务2：用 Terraform 创建腾讯云 TKE

> ⽤Terraform代码实现创建腾讯云TKE（实现代码即可）。
>
> [查看Terraform代码](./t2_terraform_tke)

1. 部署TKE：

   ```
   export TENCENTCLOUD_SECRET_ID="..."
   export TENCENTCLOUD_SECRET_KEY="..."
   export TENCENTCLOUD_REGION="ap-shanghai"
   export TF_VAR_node_password="..."

   terraform init      # 初始化项目
   terraform validate  # 校验与检查
   terraform apply     # 部署

   ```

2. 导出kubeconfig

   ```
   terraform output -raw KUBECONFIG > kubeconfig.yaml
   chmod 600 kubeconfig.yaml
   ```

3. 访问TKE集群

   ```
   export KUBECONFIG=kubeconfig.yaml
   kubectl get node
   ```

> 目前存在的问题（已解决）：
> 
> 我理解中的集群应该是像[这个 Terraform TKE 配置](./t2_terraform_tke/tke.tf.not_ok)，
> 也就是在创建集群的时候只有节点池，没有`worker_config`定义的独立节点，
> 但是这样的话，创建集群的操作会超时不成功，报错如下：
> 
> ```
> │ Error: cls-iba6kxnj create cluster endpoint vip status still is Creating
> │
> │   with tencentcloud_kubernetes_cluster.tke-jihu_test,
> │   on tke.tf line 2, in resource "tencentcloud_kubernetes_cluster" "tke-jihu_test":
> │    2: resource "tencentcloud_kubernetes_cluster" "tke-jihu_test" {
> ```
> 
> 所以，[目前](./t2_terraform_tke/tke.tf)只能也定义一个非节点池内的节点，
> 这样才可以把集群部署起来。怀疑与网络设置有关，该问题留待继续解决，同时也求助考官^_^。
>
> ====== 问题后续 ======
>
> 根据与腾讯云的沟通，确实必须至少定义一个独立节点，从而让集群先创建起来，然后才能创建节点池。

## 任务3：用 Helm Chart 部署无状态服务 Service A

> 开发Helm chart，将该服务部署到部署到指定的TKE集群。
> 
> [查看Helm Chart代码](./t3_helm_chart)

- Service 采用 LoadBalancer 类型，目前是 CLB 绑定 NodePort 的方式
  - 若要将腾讯云的负载均衡器直连到 Service A 的各个 Pod，需要集群为 CNI 网络模式（题目给出的环境不是），
    这样不经过 NodePort 和 NAT，网络转发路径更短。
- 开启了 HorizontalPodAutoscaler。
- 在 `values.yaml` 中自定义Service A的配置参数，通过 ConfigMap 挂载进去。

1. 部署服务：

   ```
   export KUBECONFIG=jihu-handson-tke-kubeconfig.yaml
   cd t3_helm_chart

   # 根据需要更新 service-a/values.yaml 中的参数

   helm lint --strict service-a                          # 检查语法
   helm package service-a                                # 打包
   helm install service-a-liukang service-a-0.1.0.tgz    # 部署
   ```

   输出如下：

   ```
   NAME: service-a-liukang
   LAST DEPLOYED: Wed Dec 15 17:22:21 2021
   NAMESPACE: default
   STATUS: deployed
   REVISION: 1
   NOTES:
   1. Get the application URL by running these commands:
        NOTE: It may take a few minutes for the LoadBalancer IP to be available.
              You can watch the status of by running 'kubectl get --namespace default svc -w service-a-liukang'
     export SERVICE_IP=$(kubectl get svc --namespace default service-a-liukang --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
     echo http://$SERVICE_IP:8888/healthz
   ```

2. 访问服务：

   ```
   export SERVICE_IP=$(kubectl get svc --namespace default service-a-liukang --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
   curl http://$SERVICE_IP:8888/healthz
   curl http://$SERVICE_IP:8888/api/v1/features
   ```

